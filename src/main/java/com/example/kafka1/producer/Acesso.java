package com.example.kafka1.producer;


public class Acesso {
    private boolean acesso;

    private int cliente;

    private int porta;

    public Acesso(int cliente, int porta) {
        this.acesso = Math.random() < 0.5;
        this.cliente = cliente;
        this.porta = porta;
    }

    public boolean isAcesso() {
        return acesso;
    }

    public void setAcesso(boolean acesso) {
        this.acesso = acesso;
    }

    public int getCliente() {
        return cliente;
    }

    public void setCliente(int cliente) {
        this.cliente = cliente;
    }

    public int getPorta() {
        return porta;
    }

    public void setPorta(int porta) {
        this.porta = porta;
    }
}
