package com.example.kafka1.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/acesso")
public class AcessoController {

    @Autowired
    AcessoProducer acessoProducer;

    @GetMapping("/{cliente}/{porta}")
    public Acesso verificarAcesso(@PathVariable int cliente, @PathVariable int porta){
        Acesso acesso = new Acesso(cliente, porta);
        acessoProducer.enviarAoKafka(acesso);
        return acesso;
    }
}
