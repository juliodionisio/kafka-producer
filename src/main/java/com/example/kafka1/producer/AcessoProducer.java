package com.example.kafka1.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class AcessoProducer {

    @Autowired
    KafkaTemplate<String, Acesso> producer;
    public void enviarAoKafka(Acesso acesso) {
        producer.send("spec3-julio-felipe-1", acesso);
    }
}
